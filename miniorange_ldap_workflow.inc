<?php
/**
 * @file
 * Contains workflow of the miniOrange Active Directory/LDAP Module.
 */

/**
 * Show workflow.
 */
function miniorange_ldap_workflow($form, &$form_state) {
  $form['markup_1'] = array(
    '#markup' => '<h3>How it works?</h3>',
  );

  $img_path = drupal_get_path('module', 'miniorange_ldap');
  $img_path = url($img_path . '/extras/drupal_gateway_diag.png');

  $form['markup_2'] = array(
    '#markup' => '<img src="' . $img_path . '" /><br>',
  );

  $form['markup_3'] = array(
    '#markup' => '<h3>Pre-requisites</h3>'
    . '<ol><li>Customer has set up the miniOrange Gateway on his DMZ Server '
    . 'and set his LDAP Configuration.</li>'
    . '<li>Customer has set his Gateway URL in the module configuration</li></ol>',
  );

  $form['markup_4'] = array(
    '#markup' => '<h3>Workflow</h3>'
    . '<ol><li>User enters his LDAP username and Password. Request is formed by'
    . ' encrypting the username<br> and password and forwarded by the module '
    . 'to miniOrange.</li>'
    . '<li>miniOrange determines that the Identity Source is LDAP and that '
    . 'the configuration is stored <br>on an On-Premise Gateway. The Gateway URL'
    . ' is retrieved and the request is forwarded to the<br> on-premise gateway.</li>'
    . '<li>The miniOrange On-Premise Gateway receives and decrypts'
    . ' the authentication request and <br>binds to the LDAP Server'
    . ' using the credentials in the request.</li>'
    . '<li>LDAP Server gives the success response if credentials are correct</li>'
    . '<li>Response is forwarded to miniOrange.</li>'
    . '<li>Response is forwarded to Wordpress. Plugin logs in the user'
    . ' if a successful response is<br> received. If user does not '
    . 'exist and auto-creation of user is enabled, the user is<br> automatically '
    . 'created</li>',
  );

  return $form;
}
