<?php
/**
 * @file
 * Contains instructions for setup of miniorange LDAP Gateway.
 */

/**
 * Form for setting up Gateway.
 */
function miniorange_ldap_gateway_form($form, &$form_state) {

  $form['markup_1'] = array(
    '#markup' => '<h3>Enter Gateway URL</h3>',
  );

  $form['miniorange_ldap_gateway_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Gateway URL'),
    '#default_value' => variable_get('miniorange_ldap_gateway_url', ''),
    '#required' => TRUE,
  );

  $form['miniorange_ldap_enable_login'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Login using LDAP for users'),
    '#default_value' => variable_get('miniorange_ldap_enable_login', FALSE),
  );

  $form['miniorange_ldap_gateway_config_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
    '#submit' => array('miniorange_ldap_gateway_configure'),
  );

  return $form;

}

/**
 * Configure miniOrange Gateway.
 */
function miniorange_ldap_gateway_configure($form, &$form_state) {

  $gateway_url = $form['miniorange_ldap_gateway_url']['#value'];
  $ldap_server_enable_login_value = $form['miniorange_ldap_enable_login']['#value'];

  if ($ldap_server_enable_login_value == 1) {
    $ldap_server_enable_login = TRUE;
  }
  else {
    $ldap_server_enable_login = FALSE;
  }

  variable_set('miniorange_ldap_enable_login', $ldap_server_enable_login);

  $gateway_config = new MiniorangeLdapGatewayConfig();
  $save_gateway_url_response = json_decode($gateway_config->saveLdapGatewayUrl($gateway_url));
  if (json_last_error() == JSON_ERROR_NONE) {
    if ($save_gateway_url_response->statusCode == 'SUCCESS') {
      variable_set('miniorange_ldap_gateway_url', $gateway_url);
      // Code to ping external gateway.
      $ping_response = json_decode($gateway_config->pingExternalGateway());
      if ($ping_response->statusCode == 'SUCCESS') {
        drupal_set_message(t('Gateway URL set to @url . @response', array('@url' => $gateway_url, '@response' => $ping_response->statusMessage)));
      }
      else {
        drupal_set_message(t('Gateway URL set to @url . @response', array('@url' => $gateway_url, '@response' => $ping_response->statusMessage)));
      }
    }
    else {
      drupal_set_message(t('An error occurred: @response', array('@response' => $save_gateway_url_response->statusMessage)), 'error');
    }
  }
  else {
    $error = array(
      '%method' => 'miniorange_ldap_gateway_configure',
      '%file' => 'miniorange_gateway_setup.inc',
      '%error' => json_last_error(),
    );
    watchdog('miniorange_ldap', "JSON Error at %method of %file: %error", $error);
  }
}
