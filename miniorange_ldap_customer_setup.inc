<?php
/**
 * @file
 * Contains form for customer setup.
 */

/**
 * Form for setting up Customer Account.
 */
function miniorange_ldap_customer_setup($form, &$form_state) {

  $current_status = variable_get('miniorange_ldap_status', '');
  if ($current_status == 'VALIDATE_OTP') {
    $form['miniorange_ldap_customer_otp_token'] = array(
      '#type' => 'textfield',
      '#title' => t('OTP'),
    );

    $form['miniorange_ldap_customer_validate_otp_button'] = array(
      '#type' => 'submit',
      '#value' => t('Validate OTP'),
      '#submit' => array('miniorange_ldap_validate_otp_submit'),
    );

    $form['miniorange_ldap_customer_setup_resendotp'] = array(
      '#type' => 'submit',
      '#value' => t('Resend OTP'),
      '#submit' => array('miniorange_ldap_resend_otp'),
    );

    $form['miniorange_ldap_customer_setup_back'] = array(
      '#type' => 'submit',
      '#value' => t('Back'),
      '#submit' => array('miniorange_ldap_back'),
    );

    return $form;
  }
  elseif ($current_status == 'LDAP_CONFIGURATION') {
    // Show customer configuration here.
    $form['markup_top'] = array(
      '#markup' => '<div>Thank you for registering with miniOrange</div>'
      . '<h4>Your Profile</h4>',
    );

    $form['markup_1'] = array(
      '#markup' => '<table border="1" style="background-color:#FFFFFF; border:1px'
      . 'solid #CCCCCC; border-collapse: collapse; padding:0px 0px 0px 10px;'
      . 'margin:2px; width:50%"><tr><td style="padding-left:5%;">Email Address</td>',
    );

    $form['customer_email'] = array(
      '#markup' => '<td style="padding-left:40%;">' . variable_get('miniorange_ldap_customer_admin_email', '')
      . '</td></tr>',
    );

    $form['markup_2'] = array(
      '#markup' => '<tr><td style="padding-left:5%;">Customer ID</td>',
    );

    $form['customer_id'] = array(
      '#markup' => '<td style="padding-left:40%;">' . variable_get('miniorange_ldap_customer_id', '') . '</td></tr>',
    );

    $form['markup_3'] = array(
      '#markup' => '<tr><td style="padding-left:5%;">Token Key</td>',
    );

    $form['customer_token'] = array(
      '#markup' => '<td style="padding-left:40%;">' . variable_get('miniorange_ldap_customer_admin_token', '') . '</td></tr>',
    );

    $form['markup_4'] = array(
      '#markup' => '<tr><td style="padding-left:5%;">API Key</td>',
    );

    $form['customer_api'] = array(
      '#markup' => '<td style="padding-left:40%;">' . variable_get('miniorange_ldap_customer_api_key', '') . '</td></tr></table>',
    );

    $form['markup_5'] = array(
      '#markup' => '<br><b>NOTE: </b>This information is required in setting up the miniOrange LDAP Gateway. Keep it handy.',
    );

    return $form;
  }

  $form['markup_14'] = array(
    '#markup' => '<h3>Register with miniOrange</h3>',
  );

  $form['markup_15'] = array(
    '#markup' => 'Just complete the short registration below to configure'
    . ' your own LDAP Server. Please enter a valid email id <br>that you have'
    . ' access to. You will be able to move forward after verifying an OTP'
    . ' that we will send to this email.',
  );

  $form['miniorange_ldap_customer_setup_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => TRUE,
  );

  $form['miniorange_ldap_customer_setup_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
  );

  $form['markup_16'] = array(
    '#markup' => '<b>NOTE:</b> We will only call if you need support.',
  );

  $form['miniorange_ldap_customer_setup_password'] = array(
    '#type' => 'password_confirm',
    '#required' => TRUE,
  );

  $form['miniorange_ldap_customer_setup_button'] = array(
    '#type' => 'submit',
    '#value' => t('Register'),
    '#submit' => array('miniorange_ldap_customer_setup_submit'),
  );

  return $form;

}

/**
 * Validate OTP.
 */
function miniorange_ldap_validate_otp_submit(&$form, $form_state) {

  $otp_token = $form['miniorange_ldap_customer_otp_token']['#value'];
  $username = variable_get('miniorange_ldap_customer_admin_email', NULL);
  $phone = variable_get('miniorange_ldap_customer_admin_phone', NULL);
  $tx_id = variable_get('miniorange_ldap_tx_id', NULL);
  $customer_config = new MiniorangeLdapCustomer($username, $phone, NULL, $otp_token);

  // Validate OTP.
  $validate_otp_response = json_decode($customer_config->validateOtp($tx_id));
  if (json_last_error() == JSON_ERROR_NONE) {
    if ($validate_otp_response->status == 'SUCCESS') {
      // OTP Validated. Show LDAP Configuration page.
      $current_status = 'LDAP_CONFIGURATION';
      variable_set('miniorange_ldap_status', $current_status);
      variable_del('miniorange_ldap_tx_id');

      // OTP Validated. Create customer.
      $password = variable_get('miniorange_ldap_customer_admin_password', '');
      $customer_config = new MiniorangeLdapCustomer($username, $phone, $password, NULL);
      $create_customer_response = json_decode($customer_config->createCustomer());
      if (json_last_error() == JSON_ERROR_NONE) {
        if ($create_customer_response->status == 'SUCCESS') {
          // Customer created.
          $current_status = 'LDAP_CONFIGURATION';
          variable_set('miniorange_ldap_status', $current_status);
          variable_set('miniorange_ldap_customer_admin_email', $username);
          variable_set('miniorange_ldap_customer_admin_phone', $phone);
          variable_set('miniorange_ldap_customer_admin_token', $create_customer_response->token);
          variable_set('miniorange_ldap_customer_id', $create_customer_response->id);
          variable_set('miniorange_ldap_customer_api_key', $create_customer_response->apiKey);

          drupal_set_message(t('Customer account created. Please read the instructions to setup your Gateway in the next section'));
        }
        else {
          drupal_set_message(t('Error creating customer'), 'error');
        }
      }
      else {
        $error = array(
          '%method' => 'miniorange_ldap_validate_otp_submit',
          '%file' => 'miniorange_ldap_customer_setup.inc',
          '%error' => json_last_error(),
        );
        watchdog('miniorange_ldap', "JSON Error at %method of %file: %error", $error);
      }
    }
    else {
      drupal_set_message(t('Error validating OTP'), 'error');
    }
  }
  else {
    $error = array(
      '%method' => 'miniorange_ldap_validate_otp_submit',
      '%file' => 'miniorange_ldap_customer_setup.inc',
      '%error' => json_last_error(),
    );
    watchdog('miniorange_ldap', "JSON Error at %method of %file: %error", $error);
  }
}

/**
 * Resend OTP.
 */
function miniorange_ldap_resend_otp(&$form, $form_state) {

  variable_del('miniorange_ldap_tx_id');
  $username = variable_get('miniorange_ldap_customer_admin_email', NULL);
  $phone = variable_get('miniorange_ldap_customer_admin_phone', NULL);
  $customer_config = new MiniorangeLdapCustomer($username, $phone, NULL, NULL);
  $send_otp_response = json_decode($customer_config->sendOtp());
  if (json_last_error() == JSON_ERROR_NONE) {
    if ($send_otp_response->status == 'SUCCESS') {
      // Store txID.
      variable_set('miniorange_ldap_tx_id', $send_otp_response->txId);
      $current_status = 'VALIDATE_OTP';
      variable_set('miniorange_ldap_status', $current_status);
      drupal_set_message(t('Verify email address by entering the passcode sent to @username', array('@username' => $username)));
    }
  }
  else {
    $error = array(
      '%method' => 'miniorange_ldap_resend_otp',
      '%file' => 'miniorange_ldap_customer_setup.inc',
      '%error' => json_last_error(),
    );
    watchdog('miniorange_ldap', "JSON Error at %method of %file: %error", $error);
  }
}

/**
 * Handle submit for customer setup.
 */
function miniorange_ldap_customer_setup_submit(&$form, $form_state) {

  $username = $form['miniorange_ldap_customer_setup_username']['#value'];
  $phone = $form['miniorange_ldap_customer_setup_phone']['#value'];
  $password = $form['miniorange_ldap_customer_setup_password']['#value']['pass1'];

  $customer_config = new MiniorangeLdapCustomer($username, $phone, $password, NULL);
  $check_customer_response = json_decode($customer_config->checkCustomer());
  if (json_last_error() == JSON_ERROR_NONE) {
    if ($check_customer_response->status == 'CUSTOMER_NOT_FOUND') {
      // Create customer.
      // Store email and phone.
      variable_set('miniorange_ldap_customer_admin_email', $username);
      variable_set('miniorange_ldap_customer_admin_phone', $phone);
      variable_set('miniorange_ldap_customer_admin_password', $password);

      $send_otp_response = json_decode($customer_config->sendOtp());
      if (json_last_error() == JSON_ERROR_NONE) {
        if ($send_otp_response->status == 'SUCCESS') {
          // Store txID.
          variable_set('miniorange_ldap_tx_id', $send_otp_response->txId);
          $current_status = 'VALIDATE_OTP';
          variable_set('miniorange_ldap_status', $current_status);
          drupal_set_message(t('Verify email address by entering the passcode sent to @username', array('@username' => $username)));
        }
      }
      else {
        $error = array(
          '%method' => 'miniorange_ldap_customer_setup_submit',
          '%file' => 'miniorange_ldap_customer_setup.inc',
          '%error' => json_last_error(),
        );
        watchdog('miniorange_ldap', "JSON Error at %method of %file: %error", $error);
      }
    }
    elseif ($check_customer_response->status == 'CURL_ERROR') {
      drupal_set_message(t('cURL is not enabled. Please enable cURL'), 'error');
    }
    else {
      // Customer exists. Retrieve keys.
      $customer_keys_response = json_decode($customer_config->getCustomerKeys());
      if (json_last_error() == JSON_ERROR_NONE) {
        variable_set('miniorange_ldap_customer_id', $customer_keys_response->id);
        variable_set('miniorange_ldap_customer_admin_token', $customer_keys_response->token);
        variable_set('miniorange_ldap_customer_admin_email', $username);
        variable_set('miniorange_ldap_customer_admin_phone', $phone);
        variable_set('miniorange_ldap_customer_api_key', $customer_keys_response->apiKey);
        $current_status = 'LDAP_CONFIGURATION';
        variable_set('miniorange_ldap_status', $current_status);
        drupal_set_message(t('Successfully retrieved your account.'));
      }
      else {
        $error = array(
          '%method' => 'miniorange_ldap_customer_setup_submit',
          '%file' => 'miniorange_ldap_customer_setup.inc',
          '%error' => json_last_error(),
        );
        watchdog('miniorange_ldap', "JSON Error at %method of %file: %error", $error);
      }
    }
  }
  else {
    $error = array(
      '%method' => 'miniorange_ldap_customer_setup_submit',
      '%file' => 'miniorange_ldap_customer_setup.inc',
      '%error' => json_last_error(),
    );
    watchdog('miniorange_ldap', "JSON Error at %method of %file: %error", $error);
  }
}

/**
 * Handle back button submit for customer setup.
 */
function miniorange_ldap_back(&$form, $form_state) {
  $current_status = 'CUSTOMER_SETUP';
  variable_set('miniorange_ldap_status', $current_status);
  variable_del('miniorange_ldap_customer_admin_email');
  variable_del('miniorange_ldap_customer_admin_phone');
  variable_del('miniorange_ldap_tx_id');
  drupal_set_message(t('Register/Login with your miniOrange Account'));
}
