<?php
/**
 * @file
 * Module file for miniOrange LDAP Module.
 */

/**
 * Implements hook_menu().
 */
function miniorange_ldap_menu() {

  $items['admin/config/people/miniorange_ldap'] = array(
    'title' => 'miniOrange Active Directory/LDAP Module Configuration',
    'description' => 'miniOrange LDAP Configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('miniorange_ldap_workflow'),
    'access arguments' => array('administer site configuration'),
    'file' => 'miniorange_ldap_workflow.inc',
  );

  $items['admin/config/people/miniorange_ldap/how_it_works'] = array(
    'title' => '1. How It Works',
    'weight' => -5,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/config/people/miniorange_ldap/customer'] = array(
    'title' => '2. Customer Setup',
    'weight' => -4,
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('administer site configuration'),
    'page arguments' => array('miniorange_ldap_customer_setup'),
    'file' => 'miniorange_ldap_customer_setup.inc',
  );

  $items['admin/config/people/miniorange_ldap/gateway_setup'] = array(
    'title' => '3. LDAP Gateway Installation',
    'weight' => -3,
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('administer site configuration'),
    'page arguments' => array('miniorange_ldap_gateway_setup'),
    'file' => 'miniorange_gateway_setup_details.inc',
  );

  $items['admin/config/people/miniorange_ldap/gateway_configuration'] = array(
    'title' => '4. Gateway Configuration',
    'weight' => -2,
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('administer site configuration'),
    'page arguments' => array('miniorange_ldap_gateway_form'),
    'file' => 'miniorange_gateway_setup.inc',
  );

  $items['admin/config/people/miniorange_ldap/testauthentication'] = array(
    'title' => '4. Test Authentication',
    'page_callback' => 'drupal_get_form',
    'weight' => -1,
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('administer site configuration'),
    'page arguments' => array('miniorange_ldap_testauthentication'),
    'file' => 'miniorange_ldap_testauthentication.inc',
  );

  return $items;
}


/**
 * Implements hook_user_login_alter().
 */
function miniorange_ldap_form_user_login_alter(&$form, $form_state) {
  $ldap_login_enabled = variable_get('miniorange_ldap_enable_login', FALSE);
  if (!empty($ldap_login_enabled)) {
    array_unshift($form['#validate'], 'miniorange_ldap_login_validate');
  }
}

/**
 * Implements hook_user_login_block_alter().
 */
function miniorange_ldap_form_user_login_block_alter(&$form, $form_state) {
  return miniorange_ldap_form_user_login_alter($form, $form_state);
}

/**
 * Form validation handler for user_login_form().
 *
 * @see user_login_form_submit()
 */
function miniorange_ldap_login_validate($form, &$form_state) {
  global $user;
  $username = $form_state['values']['name'];
  $password = trim($form_state['values']['pass']);

  if (!empty($username) && !empty($password)) {
    // Check if flood limit has been reached.
    if (!flood_is_allowed('failed_login_attempt_ip', variable_get('user_failed_login_ip_limit', 50), variable_get('user_failed_login_ip_window', 3600))) {
      $form_state['flood_control_triggered'] = 'ip';
      return;
    }

    $account = db_query("SELECT * FROM {users} WHERE name = :name AND status = 1", array(':name' => $username))->fetchObject();
    if ($account) {
      if (variable_get('user_failed_login_identifier_uid_only', FALSE)) {
        $identifier = $account->uid;
      }
      else {
        $identifier = $account->uid . '-' . ip_address();
      }
      $form_state['flood_control_user_identifier'] = $identifier;
      if (!flood_is_allowed('failed_login_attempt_user', variable_get('user_failed_login_user_limit', 5), variable_get('user_failed_login_user_window', 21600), $identifier)) {
        $form_state['flood_control_triggered'] = 'user';
        return;
      }
    }

    $customer_config = new MiniorangeLdapConfig();
    $response = json_decode($customer_config->ldapLogin($username, $password));
    if (json_last_error() == JSON_ERROR_NONE) {
      if ($response->statusCode == 'SUCCESS') {
        if ($account) {
          $user = user_load($account->uid);
          $form_state['uid'] = $user->uid;
        }
        else {
          user_external_login_register($username, 'miniorange_ldap');
          $form_state['uid'] = $user->uid;
        }
      }
    }
  }
}
