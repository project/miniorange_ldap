<?php
/**
 * @file
 * Contains form for configuring LDAP Server.
 */

/**
 * Form for LDAP Configuration.
 */
function miniorange_ldap_configuration($form, &$form_state) {

  if (variable_get('miniorange_ldap_customer_admin_email', '') != ''
    and variable_get('miniorange_ldap_customer_admin_phone', '') != ''
    and variable_get('miniorange_ldap_customer_admin_token', '') != '') {

    $ldap_server_url = MiniorangeLdapUtility::decrypt(variable_get('miniorange_ldap_server_url', ''));

    $form['markup_1'] = array(
      '#markup' => 'Enter the LDAP Server URL in the following format: <b>ldap://&lt;server/IP address&gt;:&lt;port&gt;</b>',
    );

    $form['miniorange_ldap_customer_ldap_server_url'] = array(
      '#type' => 'textfield',
      '#title' => t('LDAP Server URL'),
      '#required' => TRUE,
      '#default_value' => $ldap_server_url,
    );

    $form['miniorange_ldap_customer_ping_ldap_server'] = array(
      '#type' => 'submit',
      '#value' => t('Contact LDAP Server'),
      '#submit' => array('miniorange_ldap_ping_ldap_server_submit'),
    );

  }
  else {
    $form['customer_setup_message'] = array(
      '#markup' => '<center><h3>You have not set up your Customer Account with miniOrange. Click on the Customer Setup tab</h3></center>',
    );
  }

  return $form;

}

/**
 * Ping Server.
 */
function miniorange_ldap_ping_ldap_server_submit(&$form, $form_state) {

  $ldap_server_url = $form['miniorange_ldap_customer_ldap_server_url']['#value'];
  $encryption_key = variable_get('miniorange_ldap_customer_admin_token', '');
  variable_set('miniorange_ldap_server_url', MiniorangeLdapUtility::encrypt($ldap_server_url, $encryption_key));
  $admin_email = variable_get('miniorange_ldap_customer_admin_email', '');
  $admin_phone = variable_get('miniorange_ldap_customer_admin_phone', '');
  $customer_ldap_config = new MiniorangeLdapCustomer($admin_email, $admin_phone, NULL, NULL);
  $test_connection_response = json_decode($customer_ldap_config->pingLdapServer($admin_email, $admin_phone, $ldap_server_url));
  if (json_last_error() == JSON_ERROR_NONE) {
    if ($test_connection_response->statusCode == 'SUCCESS') {
      // Connection successful.
      drupal_set_message(t('Successfully saved LDAP Configuration. LDAP Server accessible'));
    }
    else {
      drupal_set_message(t('Error occurred: @response', array('@response' => $test_connection_response->statusMessage)), 'error');
    }
  }
  else {
    $error = array(
      '%method' => 'miniorange_ldap_ping_ldap_server_submit',
      '%file' => 'miniorange_ldap_configuration.inc',
      '%error' => json_last_error(),
    );
    watchdog('miniorange_ldap', "JSON Error at %method of %file: %error", $error);
  }
}
