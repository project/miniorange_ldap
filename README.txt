CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
Login to any Drupal site using credentials stored in Active Directory, OpenLDAP
and other LDAP servers, especially when the AD or LDAP does not have a public IP
address. In other words, this module is best utilized If the LDAP Server is not
directly accessible from your Drupal site. There are two parts to this module
- One part sits on the Drupal site and the other sits on your DMZ. Additional
benefit is that there is no need to have the PHP LDAP extension enabled, hence
better usability.

REQUIREMENTS
------------
No Special Requirements

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » People » miniOrange LDAP
 Module Configuration:


   - Setup Customer account with miniOrange


     Create/Login with miniOrange by entering email address, phone number and
     password.


   - Contact LDAP Server


     Enter LDAP Server URL(ldap://<ldap_server_url>:<port>) and try to contact
     it using the module.


   - Configure User Mapping and Bind


     This configuration determines the LDAP attributes which are bound to the
     user. It determines where the users are located in LDAP and what attributes
     are used as the username. The option to spcify usage of an external LDAP
     Gateway is also provided in this screen, along with the option to enable
     login via LDAP.

   - Test Authentication

     This page is used to mimic authentication for LDAP Users.
