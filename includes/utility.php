<?php
/**
 * @file
 * Contains utility class.
 */

/**
 * @file
 * This class represents utility methods.
 */
class MiniorangeLdapUtility {

  /**
   * Check if customer is registered.
   */
  public static function isCustomerRegistered() {
    $email = variable_get('miniorange_ldap_admin_email', NULL);
    $phone = variable_get('miniorange_ldap_admin_phone', NULL);
    $customer_key = variable_get('miniorange_ldap_admin_customer_key', NULL);
    if (!$email or !$phone or !$customer_key or !is_numeric(trim($customer_key))) {
      return 0;
    }
    else {
      return 1;
    }
  }

  /**
   * Check if value entered is null or empty.
   */
  public static function checkEmptyOrNull($value) {
    if (!isset($value) or empty($value)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Encrypt.
   */
  public static function encrypt($str) {
    $key = variable_get('miniorange_ldap_customer_admin_token', NULL);
    $block = mcrypt_get_block_size('rijndael_128', 'ecb');
    $pad = $block - (strlen($str) % $block);
    $str .= str_repeat(chr($pad), $pad);
    return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $str, MCRYPT_MODE_ECB));
  }

  /**
   * Decrypt.
   */
  public static function decrypt($value) {
    $key = variable_get('miniorange_ldap_customer_admin_token', NULL);
    $string = rtrim(
      mcrypt_decrypt(
        MCRYPT_RIJNDAEL_128,
        $key,
        base64_decode($value),
        MCRYPT_MODE_ECB,
        mcrypt_create_iv(
          mcrypt_get_iv_size(
            MCRYPT_RIJNDAEL_128,
            MCRYPT_MODE_ECB
          ),
          MCRYPT_RAND
        )
      ), "\0"
    );
    return trim($string, "\0..\32");
  }

  /**
   * Check if cURL is installed.
   */
  public static function isCurlInstalled() {
    if (in_array('curl', get_loaded_extensions())) {
      return 1;
    }
    else {
      return 0;
    }
  }

  /**
   * Check if an extension is installed.
   */
  public static function isExtensionInstalled($name) {
    if (in_array($name, get_loaded_extensions())) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
