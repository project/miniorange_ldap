<?php
/**
 * @file
 * Contains constants class.
 */

/**
 * @file
 * This class represents constants used throughout project.
 */
class MiniorangeLdapConstants {

  const BASE_URL = 'https://auth.miniorange.com';

}
