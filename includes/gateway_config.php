<?php
/**
 * @file
 * Contains class for setup of miniorange LDAP Gateway.
 */

/**
 * @file
 * This class represents gateway setup for customer.
 */
class MiniorangeLdapGatewayConfig {

  private $customerId;

  private $tokenKey;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->customerId = variable_get('miniorange_ldap_customer_id', '');
    $this->apiKey = variable_get('miniorange_ldap_customer_api_key', '');
  }

  /**
   * Save Gateway URL.
   */
  public function saveLdapGatewayUrl($gateway_url) {

    if (!MiniorangeLdapUtility::isCurlInstalled()) {
      return 'CURL_ERROR';
    }
    elseif (!MiniorangeLdapUtility::isExtensionInstalled('mcrypt')) {
      return 'MCRYPT_ERROR';
    }

    $url = MiniorangeLdapConstants::BASE_URL . '/moas/api/ldap/set-external-gateway';

    $data = $this->getGatewayConfigRequest($gateway_url);
    $data_string = json_encode($data);
    $curl = curl_init();

    /* Current time in milliseconds since midnight, January 1, 1970 UTC. */
    $current_time_in_millis = round(microtime(TRUE) * 1000);

    /* Creating the Hash using SHA-512 algorithm */
    $string_to_hash = $this->customerId . $current_time_in_millis . $this->apiKey;
    $hash_value = hash("sha512", $string_to_hash);

    $customer_key_header = "Customer-Key: " . $this->customerId;
    $timestamp_header = "Timestamp: " . $current_time_in_millis;
    $authorization_header = "Authorization: " . $hash_value;

    curl_setopt_array($curl, array(
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => $url,
      CURLOPT_POST => 1,
      CURLOPT_POSTFIELDS => $data_string,
      CURLOPT_FOLLOWLOCATION => 1,
      CURLOPT_SSL_VERIFYPEER => TRUE,
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string),
      ),
    ));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $customer_key_header,
      $timestamp_header, $authorization_header,
    ));
    $response = curl_exec($curl);

    if (curl_errno($curl)) {
      $error = array(
        '%method' => 'saveLdapGatewayUrl',
        '%file' => 'gateway_config.php',
        '%error' => curl_error($curl),
      );
      watchdog('miniorange_ldap', "cURL Error at %method of %file: %error", $error);
    }
    curl_close($curl);
    return $response;

  }

  /**
   * Get request to send to save Gateway URL.
   */
  public function getGatewayConfigRequest($gateway_url) {
    $request_type = 'Save External Gateway URL';
    $encrypted_gateway_url = MiniorangeLdapUtility::encrypt($gateway_url);
    $fields = array(
      'customerId' => $this->customerId,
      'externalGatewayUrl' => $encrypted_gateway_url,
      'ldapAuditRequest' => array(
        'endUserEmail' => MiniorangeLdapUtility::encrypt(variable_get('miniorange_ldap_customer_admin_email', '')),
        'applicationName' => $_SERVER['SERVER_NAME'],
        'appType' => 'Drupal LDAP Login Plugin',
        'requestType' => $request_type,
      ),
    );
    return $fields;
  }

  /**
   * Get LDAP Connection.
   */
  public function pingExternalGateway() {

    if (!MiniorangeLdapUtility::isCurlInstalled()) {
      return json_encode(array(
        "statusCode" => 'CURL_ERROR',
        "statusMessage" => '<a target="_blank" href="http://php.net/manual/en/curl.installation.php">PHP cURL extension</a> is not installed or disabled.',
      ));
    }

    $customer_id = variable_get('miniorange_ldap_customer_id', '');
    $application_name = $_SERVER['SERVER_NAME'];
    $admin_email = variable_get('miniorange_ldap_customer_admin_email', '');

    $app_type = 'Drupal LDAP Plugin';
    $request_type = 'Ping LDAP Through External Gateway';
    $url = MiniorangeLdapConstants::BASE_URL . '/moas/api/ldap/ping?customerId=' . urlencode($customer_id)
        . '&endUserEmail=' . urlencode($admin_email)
        . '&appType=' . urlencode($app_type) . '&requestType=' . urlencode($request_type)
        . '&applicationName=' . urlencode($application_name);
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('charset: UTF - 8', 'Authorization: Basic'));
    curl_setopt($ch, CURLOPT_POST, FALSE);
    $content = curl_exec($ch);

    if (curl_errno($ch)) {
      $error = array(
        '%method' => 'pingExternalGateway',
        '%fie' => 'gateway_config.php',
        '%error' => curl_error($ch),
      );
      watchdog('miniorange_ldap', "cURL Error at %method of %file: %error", $error);
    }
    curl_close($ch);
    return $content;
  }

}
