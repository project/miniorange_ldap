<?php
/**
 * @file
 * Contains test authentication form.
 */

/**
 * Form for Test Authentication.
 */
function miniorange_ldap_testauthentication($form, &$form_state) {

  if (variable_get('miniorange_ldap_customer_admin_email', '') != ''
    and variable_get('miniorange_ldap_customer_admin_token', '') != '') {

    $form['markup_1'] = array(
      '#markup' => '<h3>Test Authentication</h3>',
    );

    $form['miniorange_ldap_testauth_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#required' => TRUE,
    );

    $form['miniorange_ldap_testauth_password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#required' => TRUE,
    );

    $form['miniorange_ldap_testauth'] = array(
      '#type' => 'submit',
      '#value' => t('Test Authentication'),
      '#submit' => array('miniorange_ldap_test_auth'),
    );

  }
  else {
    $form['customer_setup_message'] = array(
      '#markup' => '<center><h3>You have not set up your Customer Account with miniOrange. Click on the Customer Setup tab</h3></center>',
    );
  }

  return $form;
}

/**
 * Handle test authentication submit.
 */
function miniorange_ldap_test_auth($form, &$form_state) {
  $username = $form['miniorange_ldap_testauth_username']['#value'];
  $password = $form['miniorange_ldap_testauth_password']['#value'];

  $customer_config = new MiniorangeLdapConfig();
  $testauth_response = json_decode($customer_config->ldapLogin($username, $password));
  if (json_last_error() == JSON_ERROR_NONE) {
    if ($testauth_response->statusCode == 'SUCCESS') {
      drupal_set_message(t('Authentication Successful'));
    }
    else {
      drupal_set_message(t('Authentication Unsuccessful'), 'error');
    }
  }
  else {
    $error = array(
      '%method' => 'miniorange_ldap_test_auth',
      '%file' => 'miniorange_ldap_testauthentication.inc',
      '%error' => json_last_error(),
    );
    watchdog('miniorange_ldap', "JSON Error at %method of %file: %error", $error);
  }

}
