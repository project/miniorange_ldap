<?php
/**
 * @file
 * Contains instructions for setup of miniorange LDAP Gateway.
 */

/**
 * Form for showing details for Setting up Gateway.
 */
function miniorange_ldap_gateway_setup($form, &$form_state) {

  $form['markup_1'] = array(
    '#markup' => '<h3>Instructions to Install miniOrange LDAP Gateway</h3>'
    . '<br><b>NOTE:</b> The Gateway needs to be installed on your DMZ Server'
    . ' that has access to your AD or LDAP. It needs to be publicly accessible.',
  );

  $form['markup_2'] = array(
    '#markup' => '<ol><li>Download the miniOrange LDAP Gateway by clicking this '
    . '<a href="http://miniorange.com/downloads/miniorangegateway.zip">link</a>.</li>'
    . '<li>Extract and copy in your DMZ Server.</li>'
    . '<li>Edit the <b>jetty.xml</b> in the <b>conf</b> directory to specify port on which Gateway will run.</li>'
    . '<li>Open command line and navigate to the extracted folder in the <b>bin</b> directory.</li>'
    . '<li>For Linux environment, run <b>sh start.sh &</b></li>'
    . '<li>For Windows environment, run <b>start.bat</b></li>',
  );

  $form['markup_3'] = array(
    '#markup' => '<li>After the server starts, navigate to <b>'
    . 'http://&lt;Server Name/IP&gt;/miniorangegateway</b>'
    . '<br>on your web browser.',
  );

  $form['markup_4'] = array(
    '#markup' => '<li>Enter default username as <b>admin</b> '
    . 'and password as <b>changeit</b></li>',
  );

  $form['markup_5'] = array(
    '#markup' => '<li>You will be asked to change your password. </b>',
  );

  $form['markup_6'] = array(
    '#markup' => '<li>Navigate to the <b>Keys Configuration</b> tab. Enter the '
    . '<b>Customer ID</b> and <b>Token Key</b> here and <br>click on '
    . '<b>Save</b>.</li>',
  );

  $form['markup_7'] = array(
    '#markup' => '<li>Set the <b>LDAP Server URL</b>, <b>Service Account DN</b> and'
    . '<b> Service Account Password</b> <br>in the <b>Server Configuration</b> tab.'
    . '<li>Set the <b>DN Attribute</b>, <b>Search Base</b> and '
    . '<b>Search Filter</b> in the <b>User Mapping</b> tab.</li>'
    . '<li>You can test authentication for users in the <b>Test Authentication'
    . '</b> tab.</li>',
  );

  $form['markup_8'] = array(
    '#markup' => '<li>The Gateway Setup is now complete. Set the Gateway URL in'
    . ' the next section.</li></ol><br>',
  );

  $form['miniorange_ldap_read_instructions'] = array(
    '#type' => 'checkbox',
    '#title' => t('I have read the instructions to install the Gateway'),
    '#default_value' => variable_get('miniorange_ldap_gateway_check', FALSE),
  );

  $form['miniorange_ldap_gateway_instructions_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed'),
    '#submit' => array('miniorange_ldap_gateway_proceed'),
  );

  return $form;
}

/**
 * Form submit method.
 */
function miniorange_ldap_gateway_proceed($form, &$form_state) {
  $instructions_read = $form['miniorange_ldap_read_instructions']['#value'];
  if ($instructions_read == 1) {
    variable_set('miniorange_ldap_gateway_check', TRUE);
    drupal_set_message(t('Configure your Gateway URL'));
    $form_state['redirect'] = 'admin/config/people/miniorange_ldap/gateway_configuration';
  }
  else {
    variable_set('miniorange_ldap_gateway_check', FALSE);
    drupal_set_message(t('Please follow the instructions to setup the Gateway and check the checkbox below'));
  }
}
